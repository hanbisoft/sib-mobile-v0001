import React, { Component } from "react";
import Payment from "./PaymentScreen";
import { StackNavigator } from "react-navigation";

export default (DrawNav = StackNavigator(

  {
    "입금/결제":Payment
  },
  {
    initialRouteName: "입금/결제"
  }
));
