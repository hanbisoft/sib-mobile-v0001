import React from "react";
import { StatusBar, WebView, StyleSheet,View } from "react-native";
import {
  Button,
  Text,
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Title,
  Left,
  Icon,
  Right,
  Footer,
} from "native-base";
import setting from '../Constant/setting.js';
import FooterCommon from '../component/Footers.js';
import Spinner from 'react-native-loading-spinner-overlay';
export default class Home extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = { visible: true };
  }
  showSpinner() {
    console.log('Show Spinner');
    this.setState({ visible: true });
  }

  hideSpinner() {
    console.log('Hide Spinner');
    this.setState({ visible: false });
  }

  
  render() {
    let urlSource = setting.HOME;
    return (
      <Container>
        <Content contentContainerStyle={{ flex: 1 }}>
          <Spinner
            visible={this.state.visible}
            textContent={'Loading...'}
            textStyle={{ color: '#FFF' }}
          />
          <WebView
            source={{ uri: urlSource }}
            style={{ marginTop: 20 }}
            onLoadStart={() => (this.showSpinner())}
            onLoad={() => (this.hideSpinner())}
          />
          <FooterCommon/>
        </Content>
      </Container>

    );
  }
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF'
  },
  footer: {
    paddingTop:'3%',
    backgroundColor: 'white',
    paddingBottom:'3%'
  },
  
})