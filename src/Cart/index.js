import React, { Component } from "react";
import Cart from "./CartScreen";
import { StackNavigator } from "react-navigation";
export default (DrawNav = StackNavigator(
  {
    장바구니:Cart
  },
  {
    initialRouteName: "장바구니"
  }
));
