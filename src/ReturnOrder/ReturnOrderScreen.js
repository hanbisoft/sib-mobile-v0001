import React from "react";
import {Platform,StatusBar, WebView, StyleSheet,PermissionsAndroid } from "react-native";
import {
  Button,
  Text,
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Title,
  Left,
  Icon,
  Right
} from "native-base";
import setting from '../Constant/setting.js';
import Spinner from 'react-native-loading-spinner-overlay';
import FooterCommon from '../component/Footers.js';
import AndroidWebView from 'react-native-webview-file-upload-android';
export default class ReturnOrder extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = { visible: true };
    async function requestCameraPermission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            'title': 'Cool Photo App Camera Permission',
            'message': 'Cool Photo App needs access to your camera ' +
                       'so you can take awesome pictures.'
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the camera")
        } else {
          console.log("Camera permission denied")
        }
      } catch (err) {
        console.warn(err)
      }
    }
  }

  
  showSpinner() {
    console.log('Show Spinner');
    this.setState({ visible: true });
  }

  hideSpinner() {
    console.log('Hide Spinner');
    this.setState({ visible: false });
  }

  render() {
    let urlSource = setting.RETURNORDER;
    return (
      <Container>
        <Content contentContainerStyle={{ flex: 1 }}>
          <Spinner
            visible={this.state.visible}
            textContent={'Loading...'}
            textStyle={{ color: '#FFF' }}
          />
          {Platform.select({
            android:  () => <AndroidWebView
                              source={{ uri: urlSource }}
                              onLoadStart={() => (this.showSpinner())}
                              onLoad={() => (this.hideSpinner())}
                            />,
            ios:      () => <WebView
                              source={{ uri: urlSource }}
                              onLoadStart={() => (this.showSpinner())}
                              onLoad={() => (this.hideSpinner())}
                            />
      })()}
        </Content>
        <FooterCommon/>
      </Container>

    );
  }
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF'
  },
})