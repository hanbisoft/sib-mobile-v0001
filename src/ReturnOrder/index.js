import React, { Component } from "react";
import ReturnOrder from "./ReturnOrderScreen";
import { StackNavigator } from "react-navigation";
export default (DrawNav = StackNavigator(
  {
    반품요청:ReturnOrder
  },
  {
    initialRouteName: "반품요청"
  }
));
