import React, { Component } from "react";
import Register from "./RegisterScreen";
import { StackNavigator } from "react-navigation";
export default (DrawNav = StackNavigator(
  {
    회원관리:Register
  },
  {
    initialRouteName: "회원관리"
  }
));
