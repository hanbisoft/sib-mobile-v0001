import React from "react";
import { AppRegistry, Image, StatusBar } from "react-native";
import {
  Button,
  Text,
  Container,
  List,
  ListItem,
  Content,
  Icon
} from "native-base";
// const routes = ["Home", "Order","Cart", "Reorder","Wish","Promotion","Recipe","OrderHistory","Payment","Deposithistory"];
const routes = [
  "홈", 
  "상품주문",
  "장바구니",
  "주문내역",
  "입금/결제",
  "반품내역",
  "반품요청",
  "회원관리"];

export default class SideBar extends React.Component {
  render() {
    return (
      <Container>
        <Content>
          <Image
            source={{
              uri:
                "https://raw.githubusercontent.com/GeekyAnts/NativeBase-KitchenSink/master/assets/drawer-cover.png"
            }}
            style={{
              height: 120,
              width: "100%",
              alignSelf: "stretch",
              position: "absolute"
            }}
          />
          <Image
            square
            style={{
              height: 42,
              width: 200,
              position: "absolute",
              alignSelf: "center",
              top: 40
            }}
            source={{
              uri:
                "http://devmall.ppang.biz/COM/images/s_logo_mall.png"
            }}
          />
          <List
            dataArray={routes}
            contentContainerStyle={{ marginTop: 120 }}
            renderRow={data => {
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate(data)}
                  style={{ justifyContent:'center' }}
                >
                  <Text>{data}</Text>
                </ListItem>
              );
            }}
          />
        </Content>
      </Container>
    );
  }
}
