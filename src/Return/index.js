import React, { Component } from "react";
import Return from "./ReturnScreen";
import { StackNavigator } from "react-navigation";
export default (DrawNav = StackNavigator(
  {
    반품내역:Return
  },
  {
    initialRouteName: "반품내역"
  }
));
